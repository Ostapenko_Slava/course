<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 20.07.18
 * Time: 19:37
 */

namespace app\controllers;


use app\controllers\AppController;
use app\models\Article;
use app\models\Category;

class CategoryController extends AppController
{
    public function indexAction()
    {
        if (isset($_GET["id"])) {
            $id = (int)$_GET["id"];
        } else {
            throw new \Exception("page not faund", 404);
        }
        $articles = new Article();
        $data = $articles->getArticlesByCat($id);
        $page = new Category();
        $pageData = $page->getCatById($id);
        $this->setMeta($pageData['title'], $pageData["description"], $pageData["keywords"]);
        $this->set(compact("data"));
    }
}