<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\admin;

use framework\base\Controller;
use framework\App;
use app\models\User;
class AppController extends Controller
{

    public $layout = "admin";

    public function __construct($route) {
        parent::__construct($route);
        if(!User::isAdmin() && $route['action'] != 'login-admin'){
            redirect(ADMIN.'/user/login-admin');
        }
        $this->setMeta(
            App::$app->getProperty("title"), 
            App::$app->getProperty("description"),
            App::$app->getProperty("keywords")
        );
    }

   
   

}
