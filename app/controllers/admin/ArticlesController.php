<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\admin;

use \app\models\Article;
use \app\models\Category;

/**
 * Description of ArticlesController
 *
 * @author slava
 */
class ArticlesController extends AppController
{

    public function indexAction() {
        $articles = new Article();
        $data = $articles->getAllArticles();
        $this->set(compact("data"));
    }

    public function deleteAction() {
        $id = (int) $_GET['id'];
        $article = new Article();
        $action = $article->delete($id, TRUE);
        redirect();
    }

    public function showAction() {
        $id = (int) $_GET['id'];
        if (!empty($_POST)) {
            $article = new Article();
            $data = $_POST;
            if (!$valid_data = $article->validate($data)) {
                $_SESSION['errors'] = $article->getErrors();
                $_SESSION['form_data'] = $data;
            } else {
                //check file and upload file
                if (!empty($_FILES['userfile']['name'])) {
                    $valid_data['userfile'] = md5(time()) . ".jpg";
                    move_uploaded_file($_FILES['userfile']['tmp_name'], WWW . '/images/' . $valid_data['userfile']);
                    //chenges permission
                } else {
                    $valid_data['userfile'] = false;
                }
                //add article
                //$valid_data['user_id'] = $_SESSION['user_id'];

                if ($article->updateAdmin($valid_data, $id)) {

                    $_SESSION['success'] = 'статья обновлена ';
                } else {
                    $_SESSION['errors'][] = 'ошибка при обновлении стати';
                }
            }
        }
        $modelArticle = new Article();
        $article = $modelArticle->getArticleForEdit($id, TRUE);
        $catModel = new Category();
        $categories = $catModel->getCatigories();
        $this->set(compact('article', 'categories'));
    }

}
