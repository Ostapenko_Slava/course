<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\admin;
use app\models\User;

/**
 * Description of UserController
 *
 * @author slava
 */
class UserController extends AppController
{
    public function loginAdminAction() {
        if(!empty($_POST)){
            $password = $_POST['password'];
            $email = $_POST['login'];
            $user= new User();
            if(!$user->loginAdmin($email,$password)){
                $_SESSION['errors'] = ['login or password fail'];
            } else {
                redirect(ADMIN);
            }
        }
        $this->layout = 'login';
    }
    public function logoutAdminAction(){
        session_destroy();
        redirect('/admin');
    }

}
