<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use framework\base\Model;
use framework\libs\Pagination;

/**
 * Description of Comment
 *
 * @author slava
 */
class Comment extends Model
{

    public function getComments($id) {
        $query = "SELECT id FROM comments WHERE article_id = ? AND status = 'active'";
        $data = $this->allRows($query, [$id]);
        return count($data);
    }

    public function getCommentsPage(Pagination $pagination, $perPage, $id) {
        $start = $pagination->getStart();
        $query = "SELECT id,name,email,text,date FROM comments WHERE article_id = ? AND status = 'active' ";
        $query .= "ORDER BY date DESC LIMIT $start, $perPage";
        $data = $this->allRows($query, [$id]);
        return $data;
    }
    public function getAllComments() {
        $query = "SELECT id,email,text,date FROM comments ORDER BY date DESC";
        $data = $this->allRows($query);
        return $data;
    }

    public function deleteComment($id) {
        $query = "DELETE FROM comments WHERE id=? LIMIT 1";
        $result = $this->deleteRow($query, [$id]);
        return true;
    }

    public function validateComment($data) {
        $data['username'] = strip_tags(trim($data['username']));
        if (mb_strlen($data['username']) == 0) {
            $this->errors[] = 'вы не ввели имя';
        }
        if (!preg_match("/^[A-Za-z]+$/", $data['username'])) {
            $this->errors[] = 'вы ввели не допустимый символ';
        }
        $data['text'] = strip_tags(trim($data['message']));
        if (mb_strlen($data['message']) == 0) {
            $this->errors[] = 'вы не ввели text';
        }
        if (!preg_match("/^[a-zA-Z0-9_\-.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-.]+$/", $data['useremail'])) {
            $this->errors[] = 'вы ввели не допустимый email';
        }
        if (!empty($this->errors)) {
            return false;
        }
        return $data;
    }

    public function save($data) {

        $query = "INSERT INTO   comments (name, email, text,article_id) VALUE (?,?,?,?)";
        $result = $this->insertRow($query, [$data['username'], $data['useremail'],
            $data['message'], $data['articleid']]);
        return $result;
    }

}
