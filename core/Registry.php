<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 29.06.18
 * Time: 20:00
 */

namespace framework;


class Registry
{
    static private $properties = [];
    use TSingleton;

    /**
     * @param array $properties
     */
    public function setProperty($name, $value)
    {
        self::$properties[$name] = $value;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return self::$properties;
    }

    public function getProperty($name)
    {
        if(isset(self::$properties[$name])){
            return self::$properties[$name];
        }
        return null;
    }
}